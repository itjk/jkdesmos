#
#   _ _       _
#  (_) | ____| | ___  ___ _ __ ___   ___  ___
#  | | |/ / _` |/ _ \/ __| '_ ` _ \ / _ \/ __|
#  | |   < (_| |  __/\__ \ | | | | | (_) \__ \
# _/ |_|\_\__,_|\___||___/_| |_| |_|\___/|___/
#|__/
#
# (C) 2020 itjk under the MIT license
# see included LICENSE file for more details
import json, os, strformat, colors, math, sequtils, strutils

template `^`(json: JsonNode): float = getFloat(json)

type
    ShapeKind = enum
        Rect = 1
        RotatedRect = 2
        Triangle = 4
        Ellipse = 8
        Circle = 32
        Line = 64
        Bezier = 128
        PolyLine = 256

# return the latex for a polygon
proc poly(node: JsonNode, c: openarray[(float, float)]): string =
    var corners: string
    for i in 0..c.high:
        corners = corners & fmt"""\left({c[i][0]}, \ {c[i][1] * -1}\right)"""
        if i < c.high:
            corners = corners & """, \ """
    
    result = "" &
        r"\operatorname{polygon}" &
        r"\left(" &
            corners &
        r"\right)"

# node[0] : left x
# node[1] : lower y
# node[2] : right x
# node[3] : upper y
proc rect(node: JsonNode): string =
    let c = [ # corners
        (^node[0], ^node[1]),
        (^node[0], ^node[3]),
        (^node[2], ^node[3]),
        (^node[2], ^node[1])
    ]
    
    result = poly(node, c)

# node[0] : left x
# node[1] : lower y
# node[2] : right x
# node[3] : upper y
# node[4] : angle
proc rotatedRect(node: JsonNode): string =
    var c = [
        (^node[0], ^node[1]),
        (^node[0], ^node[3]),
        (^node[2], ^node[3]),
        (^node[2], ^node[1])
    ]

    let cx = (c[0][0] + c[2][0]) / 2
    let cy = (c[0][1] + c[2][1]) / 2

    for i in 0..c.high:
        let tempx = c[i][0] - cx
        let tempy = c[i][1] - cy

        let rotatedx = (tempx * cos degToRad(^node[4])) - (tempy * sin degToRad(^node[4]))
        let rotatedy = (tempx * sin degToRad(^node[4])) + (tempy * cos degToRad(^node[4]))

        c[i][0] = rotatedx + cx
        c[i][1] = rotatedy + cy

    result = poly(node, c)

# node[0], node[1] : x, y
# node[2], node[3] : x, y
# node[4], node[5] : x, y
proc triangle(node: JsonNode): string =
    let c = [ # corners
        (^node[0], ^node[1]),
        (^node[2], ^node[3]),
        (^node[4], ^node[5])
    ]
    
    result = poly(node, c)

# node[0] : x point (h)
# node[1] : y point (k)
# node[2] : radius (r)
proc circle(node: JsonNode): string =
    let h = ^node[0]
    let k = ^node[1] * -1
    let r = ^node[2]

    # (x-h)^2 + (y-k)^2 < r^2
    let equation = r"\left(x-{h}\right)^2+\left(y-{k}\right)^2<{r}^2".fmt()
    # to remove lines, using bounds
    # circle{circle} (bounds)
    result = r"[equation]\left\{[equation]\right\}".fmt('[', ']')

# node[0] : x point
# node[1] : y point
# node[2] : radius on x axis (a)
# node[3] : radius on y axis (b)
proc ellipse(node: JsonNode): string =
    let h = ^node[0]
    let k = ^node[1] * -1
    let a = ^node[2]
    let b = ^node[3]

    # (x-h^2/a^2) + (y-k^2/b^2) < 1
    let equation = r"\frac{\left(x-[h]\right)^2}{[a]^2}+\frac{\left(y-[k]\right)^2}{[b]^2}<1".fmt('[', ']')
    result = r"[equation]\left\{[equation]\right\}".fmt('[', ']')

# node[0, 1] : x, y
# node[2, 3] : x, y
proc line(node: JsonNode): (seq[float], seq[float]) =
    let x = @[^node[0], ^node[2]]
    let y = @[- ^node[1], - ^node[3]]
    result = (x, y)

# node [0, 1]: x, y
# node [2, 3]: x, y
# node [4, 5]: x, y
proc bezier(node: JsonNode): string =
    let x1 = ^node[0]
    let y1 = ^node[1]
    let x2 = ^node[2]
    let y2 = ^node[3]
    let x3 = ^node[4]
    let y3 = ^node[5]

    # i hate latex
    # https://en.wikipedia.org/wiki/B%C3%A9zier_curve
    # https://math.stackexchange.com/questions/1360891/find-quadratic-bezier-curve-equation-based-on-its-control-points
    # x = (x1 - 2x2 - x3)t^2 + 2(x2-x1)t + x1
    # y = (y1 - 2y2 - y3)t^2 + 2(y2-y1)t + y1
    # y is multiplied by -1 because image is upside down
    result = r"""
        \left(
            \left({x1}-2\left({x2}\right)+{x3}\right)t^2 + 2\left({x2}-{x1}\right)t + {x1},
            -1 * \left(\left({y1}-2\left({y2}\right)+{y3}\right)t^2 + 2\left({y2}-{y1}\right)t + {y1}\right)
        \right)
    """.fmt().unindent()

# node[0, 1] : x, y
# node[2, 3] : x, y..
proc polyline(node: JsonNode): (seq[float], seq[float]) =
    var x, y: seq[float]
    for i in node.items:
        if x.len > y.len: y.add(- ^i)
        else: x.add(^i)
    result = (x, y)


proc polyout(i: int, latex: string, colorarr: JsonNode, lines = false): string =
    "\n" &
    "calculator.setExpression(" & $(%*{
        "id": $i ,
        "latex": latex,
        "lines": $lines,
        "fillOpacity": $(colorarr[3].getInt() / 255),
        "color": $rgb(colorarr[0].getInt(), colorarr[1].getInt(), colorarr[2].getInt())
    }) & ")"

proc bezierout(i: int, latex: string, colorarr: JsonNode, lines = false): string =
    "\n" &
    "calculator.setExpression(" & $(%*{
        "id": $i ,
        "latex": latex,
        "lines": $lines,
        "fillOpacity": $(colorarr[3].getInt() / 255),
        "color": $rgb(colorarr[0].getInt(), colorarr[1].getInt(), colorarr[2].getInt()),
        "domain": %*{"min": "0", "max": "1"},
        "parametricDomain": %*{"min": "0", "max": "1"}
    }) & ")"

proc tableout(i: int, values: (seq[float], seq[float]), colorarr: JsonNode): string =
    let color = $rgb(colorarr[0].getInt(), colorarr[1].getInt(), colorarr[2].getInt())
    "\n" &
    "calculator.setExpression(" & $(%*{
        "id": $i,
        "type": "table",
        "columns": [
            {
                "id": $(i.float-0.2),
                "color": color,
                "hidden": true,
                "latex": fmt("x_{[$i]}", '[', ']'),
                "values": values[0].mapIt($it)
            },
            {
                "id": $(i.float-0.4),
                "color": color,
                "latex": fmt("y_{[$i]}", '[', ']'),
                "values": values[1].mapIt($it),
                "points": false,
                "lines": true
            }
        ]
    }) & ")"

var infile: File
var input: string

# open input file
if infile.open(getAppDir() / "input.json"):
    input = infile.readAll()
else:
    echo "input.json does not exist!"
    quit QuitFailure

var output: string
let shapes = parseJson(input)["shapes"]

var i = 0
for node in shapes:
    var latex: string
    inc i
    
    let colorarr = node["color"]

    case (ShapeKind node["type"].getInt()):
        of Rect:
            latex = rect(node["data"])
            output = output & polyout(i, latex, colorarr)
        of RotatedRect:
            latex = rotatedRect(node["data"])
            output = output & polyout(i, latex, colorarr)
        of Triangle:
            latex = triangle(node["data"])
            output = output & polyout(i, latex, colorarr)
        of Ellipse: 
            latex = ellipse(node["data"])
            output = output & polyout(i, latex, colorarr, true)
        of Circle:
            latex = circle(node["data"])
            output = output & polyout(i, latex, colorarr, true)
        of Line:
            let tup = line(node["data"])
            output = output & tableout(i, tup, colorarr)
        of Bezier:
            latex = bezier(node["data"])
            output = output & bezierout(i, latex, colorarr, true)
        of PolyLine: 
            let tup = polyline(node["data"])
            output = output & tableout(i, tup, colorarr)
    

output = """
<script src="https://www.desmos.com/api/v1.5/calculator.js?apiKey=dcb31709b452b1cf9dc26972add0fda6"></script>

<div id="calculator" style="width: 1200px; height: 800px;"></div>

<script>
var elt = document.getElementById('calculator');
var calculator = Desmos.GraphingCalculator(elt);

calculator.setExpression({
    type: "text",
    id: "credit_note",
    text: "created by cricket \n https://gitlab.com/itjk \n\n with the help of jkDesmos https://gitlab.com/itjk/jkdesmos"
})
""" & output &
"""
</script>
"""

echo output
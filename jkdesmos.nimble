# Package
version       = "0.1.0"
author        = "jan Kiwikete"
description   = "personal desmos helper"
license       = "MIT"
srcDir        = "src"
bin           = @["jkdesmos"]

# Dependencies
requires "nim >= 1.4.0"